var _____WB$wombat$assign$function_____ = function (name) {
    return (self._wb_wombat && self._wb_wombat.local_init && self._wb_wombat.local_init(name)) || self[name];
};
if (!self.__WB_pmw) {
    self.__WB_pmw = function (obj) {
        this.__WB_source = obj;
        return this;
    }
}
{
    let window = _____WB$wombat$assign$function_____("window");
    let self = _____WB$wombat$assign$function_____("self");
    let document = _____WB$wombat$assign$function_____("document");
    let location = _____WB$wombat$assign$function_____("location");
    let top = _____WB$wombat$assign$function_____("top");
    let parent = _____WB$wombat$assign$function_____("parent");
    let frames = _____WB$wombat$assign$function_____("frames");
    let opener = _____WB$wombat$assign$function_____("opener");

    Ajax.InPlaceExternalControlledEditor = Class.create();
    Ajax.InPlaceExternalControlledEditor.defaultHighlightColor = "#ffffff";

    Object.extend(Ajax.InPlaceExternalControlledEditor.prototype, Ajax.InPlaceEditor.prototype);
    Object.extend(Ajax.InPlaceExternalControlledEditor.prototype, {
        initialize: function (element, url, options) {
            this.url = url;
            this.element = $(element);

            this.options = Object.extend({
                paramName: "value",
                okButton: true,
                okText: "ok",
                cancelLink: true,
                cancelText: "cancel",
                savingText: "Saving...",
                clickToEditText: "Click to edit",
                okText: "ok",
                rows: 1,
                onComplete: function (transport, element) {
                    new Effect.Highlight(element, {startcolor: this.options.highlightcolor});
                },
                onFailure: function (transport) {
                    alert("Error communicating with the server: " + transport.responseText.stripTags());
                },
                callback: function (form) {
                    return Form.serialize(form);
                },
                handleLineBreaks: true,
                loadingText: 'Loading...',
                savingClassName: 'inplaceeditor-saving',
                loadingClassName: 'inplaceeditor-loading',
                formClassName: 'inplaceeditor-form',
                highlightcolor: "transparent",
                highlightendcolor: "transparent",
                externalControl: null,
                submitOnBlur: false,
                ajaxOptions: {},
                evalScripts: false
            }, options || {});

            if (!this.options.formId && this.element.id) {
                this.options.formId = this.element.id + "-inplaceeditor";
                if ($(this.options.formId)) {
                    // there's already a form with that name, don't specify an id
                    this.options.formId = null;
                }
            }

            if (this.options.externalControl) {
                this.options.externalControl = $(this.options.externalControl);
            }

            this.originalBackground = Element.getStyle(this.element, 'background-color');
            if (!this.originalBackground) {
                this.originalBackground = "transparent";
            }

            this.element.title = this.options.clickToEditText;

            this.onclickListener = this.enterEditMode.bindAsEventListener(this);
            this.mouseoverListener = this.enterHover.bindAsEventListener(this);
            this.mouseoutListener = this.leaveHover.bindAsEventListener(this);
            if (this.options.externalControl) {
                Event.observe(this.options.externalControl, 'click', this.onclickListener);
                Event.observe(this.options.externalControl, 'mouseover', this.mouseoverListener);
                Event.observe(this.options.externalControl, 'mouseout', this.mouseoutListener);
            } else {
                Event.observe(this.element, 'click', this.onclickListener);
                Event.observe(this.element, 'mouseover', this.mouseoverListener);
                Event.observe(this.element, 'mouseout', this.mouseoutListener);
            }
        },
        getText: function () {
            return new String(this.element.innerHTML).unescapeHTML();
        },
    });


    function setSelectionRange(input, selectionStart, selectionEnd) {
        input.focus();
        input.setSelectionRange(selectionStart, selectionEnd);
    }

    function replaceSelection(input, replaceString) {
        var selectionStart = input.selectionStart;
        var selectionEnd = input.selectionEnd;
        var contentBeforeSelection = input.value.substring(0, selectionStart);
        var contentAfterSelection = input.value.substring(selectionEnd);
        var codeRegExp = /[\r\n]+\{{3}[^\}{3}]*$/;

        if (codeRegExp.test(contentBeforeSelection)) {
            input.value = contentBeforeSelection + replaceString + contentAfterSelection;
            if (selectionStart != selectionEnd) {
                setSelectionRange(input, selectionStart, selectionStart + replaceString.length);
            } else {
                setSelectionRange(input, selectionStart + replaceString.length, selectionStart + replaceString.length);
            }
            return true;
        }
        return false;
    }

    function catchTab(item, e) {
        if (e.which == 9 && item.setSelectionRange) {
            scrollPos = item.scrollTop;
            if (replaceSelection(item, '  ')) {
                setTimeout("document.getElementById('" + item.id + "').focus();document.getElementById('" + item.id + "').scrollTop=scrollPos;", 0);
                return false;
            }
            return true;
        }
    }


}
/*
     FILE ARCHIVED ON 22:10:04 Oct 01, 2011 AND RETRIEVED FROM THE
     INTERNET ARCHIVE ON 09:11:02 Jun 08, 2022.
     JAVASCRIPT APPENDED BY WAYBACK MACHINE, COPYRIGHT INTERNET ARCHIVE.

     ALL OTHER CONTENT MAY ALSO BE PROTECTED BY COPYRIGHT (17 U.S.C.
     SECTION 108(a)(3)).
*/
/*
playback timings (ms):
  captures_list: 243.274
  exclusion.robots: 0.182
  exclusion.robots.policy: 0.174
  cdx.remote: 0.064
  esindex: 0.009
  LoadShardBlock: 188.343 (3)
  PetaboxLoader3.datanode: 179.497 (4)
  CDXLines.iter: 15.988 (3)
  load_resource: 192.418
  PetaboxLoader3.resolve: 49.31
*/
